# INSTALL

For a released core package, just use the normal way of installing, by using the
`package/package_native_index.json` file.

For installing by hand, there are the following options:
1. Install for IDE
1. Install for CLI
1. Install in sketch

## INSTALL FOR IDE

Put a reference to a local copy of `package_native_index.json` in the
usual "board manager URL's" section, by using the file protocol, with four
slashes: "file:////absolute-path-to-package_native_index.json".

Then copy the files as described in the following "ARDUINO15 LAYOUT"
section.

## INSTALL FOR CLI

Put a reference to a local copy of `package_native_index.json` in the
usual "~/.arduino15/arduino-cli.yaml" file, by using the file protocol, with
three slashes: "file:///absolute-path-to-package_native_index.json".

Then copy the files as described in the following "ARDUINO15 LAYOUT"
section.

## INSTALL IN SKETCH

It is also possible to install in the sketch folder, which is simpler:
* Copy in `~/Arduino/hardware/native/native/`:
    * `board.txt`
    * `core`
    * `libraries`
    * `platform.txt`
    * `programers.txt`
    * `tools`
    * `variant`

The script `install_linux.sh` will perform all the required copies, and it can
also remove copied files.

## ARDUINO15 LAYOUT

To install in `.arduino15` folder, the layout must be the following:
* In `.arduino.15`:
    * `package_native_index.json`
* In `.arduino15/packages/native/hardware/native/<version>`:
    * `board.txt`
    * `core`
    * `libraries`
    * `platform.txt`
    * `programers.txt`
    * `tools`
    * `variant`

The script `install_linux.sh` will perform all the required copies, and it can
also remove copied files.

## CHECK INSTALLATION

* IDE: just re-open and ceck whether the native core is available in the boards
    selector
* CLI: run `arduino-cli board listall | grep native` and see if it returns the
    native boards
