#include "../../include/arduinoLib/arduinoPins.hpp"
#include "../../include/arduinoNative/Simulator.hpp"

using arduinoNative::PinMode;
using arduinoNative::Simulator;

namespace /*anon*/ {
PinMode mapMode(const uint8_t mode)
{
    switch (mode)
    {
        case arduinoLib::INPUT: return PinMode::MODE_INPUT;
        case arduinoLib::INPUT_PULLUP: return PinMode::MODE_INPUT_PULLUP;
        case arduinoLib::OUTPUT: return PinMode::MODE_OUTPUT;
    }
    return PinMode::MODE_INVALID;
}

} // anon

namespace arduinoLib {

void pinMode(const uint8_t pin, const uint8_t mode)
{
    auto & sim = Simulator::getInstance();
    sim.getPin(pin).setMode(mapMode(mode));
}

void digitalWrite(const uint8_t pin, const uint8_t value)
{
    auto & sim = Simulator::getInstance();
    sim.getPin(pin).write(value != 0);
}

int digitalRead(const uint8_t pin)
{
    auto & sim = Simulator::getInstance();
    return sim.getPin(pin).read();
}

} // arduino
