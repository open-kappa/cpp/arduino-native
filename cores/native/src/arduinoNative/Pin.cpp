#include "../../include/arduinoNative/Pin.hpp"
#include "../../include/arduinoNative/Simulator.hpp"

using arduinoNative::Pin;
using arduinoNative::PinMode;
using arduinoNative::PinNumber;

Pin::~Pin()
{}

Pin::Pin(const PinNumber number):
    IComponent(),
    _number(number),
    _mode(PinMode::MODE_UNINITIALIZED),
    _value(false)
{}

PinNumber Pin::getNumber() const
{
    return _number;
}

PinMode Pin::getMode() const
{
    return _mode;
}

void Pin::setMode(const PinMode mode)
{
    const auto oldMode = _mode;
    _mode = mode;
    auto & board = _simulator->getBoard();
    if (!board.pinCheckMode(*this))
    {
        _simulator->getPolicy().pinEvent(
            *this,
            _mode == PinMode::MODE_INPUT
            ? PinEvent::PIN_INVALID_INITIALIZE_INPUPT
            : _mode == PinMode::MODE_INPUT_PULLUP
            ? PinEvent::PIN_INVALID_INITIALIZE_INPUPT_PULLUP
            : _mode == PinMode::MODE_OUTPUT
            ? PinEvent::PIN_INVALID_INITIALIZE_OUTPUT
            : PinEvent::PIN_INVALID_INITIALIZE_VALUE
        );
        if (_mode == PinMode::MODE_INVALID) _mode = mode;
    }
    else if (_mode == oldMode)
    {
        _simulator->getPolicy().pinEvent(
            *this,
            PinEvent::PIN_DOUBLE_INITIALIZE
        );
    }
    if (oldMode != PinMode::MODE_UNINITIALIZED
        && oldMode != PinMode::MODE_DEFAULT_INPUT
        && oldMode != PinMode::MODE_DEFAULT_INPUT_PULLUP
        && oldMode != PinMode::MODE_DEFAULT_OUTPUT
        && !board.pinCanReinitialize(*this)
    )
    {
        _simulator->getPolicy().pinEvent(
            *this,
            PinEvent::PIN_INVALID_REINITIALIZE
        );
    }
}

bool Pin::read() const
{
    if (_mode == PinMode::MODE_UNINITIALIZED)
    {
        _simulator->getPolicy().pinEvent(
            *this,
            PinEvent::PIN_UNINITIALIZED_READ
        );
    }
    else if (_mode != PinMode::MODE_DEFAULT_INPUT
        && _mode != PinMode::MODE_INPUT
        && _mode != PinMode::MODE_DEFAULT_INPUT_PULLUP
        && _mode != PinMode::MODE_INPUT_PULLUP
        )
    {
        _simulator->getPolicy().pinEvent(
            *this,
            PinEvent::PIN_INVALID_READ
        );
    }
    return _value;
}

void Pin::write(const bool value)
{
    const auto oldValue = _value;
    _value = value;
    if (_mode == PinMode::MODE_UNINITIALIZED)
    {
        _simulator->getPolicy().pinEvent(
            *this,
            PinEvent::PIN_UNINITIALIZED_WRITE
        );
    }
    else if (_mode != PinMode::MODE_DEFAULT_OUTPUT
        && _mode != PinMode::MODE_OUTPUT
        )
    {
        _simulator->getPolicy().pinEvent(
            *this,
            PinEvent::PIN_INVALID_WRITE
        );
    }

    if (oldValue == _value)
    {
         _simulator->getPolicy().pinEvent(
            *this,
             oldValue
             ? PinEvent::PIN_DOUBLE_WRITE_TRUE
             : PinEvent::PIN_DOUBLE_WRITE_FALSE
        );
    }
}

void Pin::initialize(
    const bool value,
    const PinMode mode
)
{
    _value = value;
    _mode = mode;
}

bool Pin::operator <(const Pin & other) const
{
    return _number < other._number;
}
