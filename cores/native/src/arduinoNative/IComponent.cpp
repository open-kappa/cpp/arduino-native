#include "../../include/arduinoNative/IComponent.hpp"
#include "../../include/arduinoNative/Simulator.hpp"

using arduinoNative::IComponent;

IComponent::~IComponent()
{}

void IComponent::setSimulator(Simulator * simulator)
{
    _simulator = simulator;
}

IComponent::IComponent():
    _simulator(nullptr)
{}

IComponent::IComponent(const IComponent & other):
    _simulator(other._simulator)
{}

IComponent::IComponent(IComponent && other):
    _simulator(std::move(other._simulator))
{}

void IComponent::swap(IComponent & other)
{
    using std::swap;
    swap(_simulator, other._simulator);
}
