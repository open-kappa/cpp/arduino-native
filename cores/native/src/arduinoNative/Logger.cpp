#include "../../include/arduinoNative/Logger.hpp"
#include <iostream>
#include <string>
#include "../../include/arduinoNative/Pin.hpp"

using arduinoNative::Logger;
using arduinoNative::Pin;
using arduinoNative::PinEvent;
using arduinoNative::SimulationEvent;

namespace /*anon*/ {

void _doPrint(
        std::ostream & ss,
        const std::string & pre,
        const SimulationEvent event)
{
    std::string msg;
    switch (event)
    {
        case SimulationEvent::COMPLETED:
            msg = "Simulation completed";
            break;
        case SimulationEvent::PAUSED:
            msg = "Simulation paused";
            break;
        case SimulationEvent::RESUMED:
            msg = "Simulation resumed";
            break;
        case SimulationEvent::START:
            msg = "Simulation starting...";
            break;
    }
    ss << pre << " " << msg << std::endl;
}

void _doPrint(
        std::ostream & ss,
        const std::string & pre,
        const Pin & pin,
        const PinEvent event)
{
    std::string msg;
    switch (event)
    {
        case PinEvent::PIN_UNINITIALIZED_WRITE:
            msg = "write on uninitialized pin";
            break;
        case PinEvent::PIN_UNINITIALIZED_READ:
            msg = "read from uninitialized pin";
            break;
        case PinEvent::PIN_INVALID_WRITE:
            msg = "write on input pin";
            break;
        case PinEvent::PIN_INVALID_READ:
            msg = "read on output pin";
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_INPUPT:
            msg = "cannot initialize pin as input";
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_INPUPT_PULLUP:
            msg = "cannot initialize pin as input pullup";
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_OUTPUT:
            msg = "cannot initialize pin as output";
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_VALUE:
            msg = "pin mode set to invalid mode value";
            break;
        case PinEvent::PIN_INVALID_REINITIALIZE:
            msg = "cannot reinitialize pin";
            break;
        case PinEvent::PIN_DOUBLE_WRITE_FALSE:
            msg = "double write of 'false' on pin";
            break;
        case PinEvent::PIN_DOUBLE_WRITE_TRUE:
            msg = "double write of 'true' on pin";
            break;
        case PinEvent::PIN_DOUBLE_INITIALIZE:
            msg = "double initialize of pin";
            break;
    }
    ss << pre << "Pin " << pin.getNumber() << " " << msg << std::endl;
}

} // anon

Logger::Logger():
    IComponent()
{}

Logger::~Logger()
{}

void Logger::log(const SimulationEvent event)
{
    _doPrint(std::cout, "[info]", event);
}

void Logger::warn(const SimulationEvent event)
{
    _doPrint(std::cout, "[info]", event);
}

void Logger::error(const SimulationEvent event)
{
    _doPrint(std::cout, "[info]", event);
}

void Logger::log(const Pin & pin, const PinEvent event)
{
    _doPrint(std::cout, "[info]", pin, event);
}

void Logger::warn(const Pin & pin, const PinEvent event)
{
    _doPrint(std::cerr, "[warn]", pin, event);
}

void Logger::error(const Pin & pin, const PinEvent event)
{
    _doPrint(std::cerr, "[erro]", pin, event);
}
