#include "../../include/arduinoNative/IPolicy.hpp"
#include "../../include/arduinoNative/Simulator.hpp"

using arduinoNative::IPolicy;

IPolicy::~IPolicy()
{}

IPolicy::IPolicy():
    IComponent()
{}

IPolicy::IPolicy(const IPolicy & other):
    IComponent(other)
{}

IPolicy::IPolicy(IPolicy && other):
    IComponent(std::move(other))
{}

void IPolicy::swap(IPolicy & other)
{
    using std::swap;
    IComponent::swap(other);
}
