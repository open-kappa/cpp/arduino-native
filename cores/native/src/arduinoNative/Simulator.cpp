#include "../../include/arduinoNative/Simulator.hpp"
#include <csignal>
#include "../../include/arduinoLib/prototypes.hpp"

using arduinoNative::IBoard;
using arduinoNative::IPolicy;
using arduinoNative::Logger;
using arduinoNative::Pin;
using arduinoNative::Simulator;

namespace /*anon*/ {

extern "C" void _onSigIntFoo(int /*sig*/)
{
    std::signal(SIGINT, _onSigIntFoo);
    Simulator::getInstance()._onSigInt();
}

extern "C" void _onSigTermFoo(int /*sig*/)
{
    std::signal(SIGTERM, _onSigTermFoo);
    Simulator::getInstance()._onSigTerm();
}

} // anon

Simulator & Simulator::getInstance()
{
    static Simulator sim;
    return sim;
}

void Simulator::run()
{
    ::setup();
    while (_continue)
    {
        ::loop();
    }
}

void Simulator::_onSigInt()
{
    _logger.log(SimulationEvent::PAUSED);
}

void Simulator::_onSigTerm()
{
    _continue = false;
}

IPolicy & Simulator::getPolicy()
{
    return *_policy;
}

void Simulator::setPolicy(IPolicy * policy)
{
    delete _policy;
    _policy = policy;
}

IBoard & Simulator::getBoard()
{
    return *_board;
}

void Simulator::setBoard(IBoard * board)
{
    _board = board;
}

Logger & Simulator::getLogger()
{
    return _logger;
}

Pin & Simulator::getPin(const PinNumber number)
{
    auto it = _pins.find(number);
    if (it == _pins.end())
    {
        auto pin = std::make_shared<Pin>(number);
        pin->setSimulator(this);
        _board->pinInit(*pin);
        _pins[number] = pin;
        it = _pins.find(number);
    }
    return const_cast<Pin &>(*it->second);
}

Simulator::Simulator():
    _continue(true),
    _pins(),
    _policy(nullptr),
    _board(nullptr),
    _logger()
{
    std::signal(SIGINT, _onSigIntFoo);
    std::signal(SIGTERM, _onSigTermFoo);
}

Simulator::~Simulator()
{
    delete _policy;
    delete _board;
}
