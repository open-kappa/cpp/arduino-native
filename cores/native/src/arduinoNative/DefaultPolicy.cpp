#include "../../include/arduinoNative/DefaultPolicy.hpp"
#include "../../include/arduinoNative/Simulator.hpp"

using arduinoNative::DefaultPolicy;

DefaultPolicy::DefaultPolicy():
    IPolicy()
{}

DefaultPolicy::~DefaultPolicy()
{}

DefaultPolicy::DefaultPolicy(const DefaultPolicy & other):
    IPolicy(other)
{}

DefaultPolicy::DefaultPolicy(DefaultPolicy && other):
    IPolicy(std::move(other))
{}

void DefaultPolicy::swap(DefaultPolicy & other)
{
    using std::swap;
    IPolicy::swap(other);
}

DefaultPolicy & DefaultPolicy::operator =(const DefaultPolicy & other)
{
    DefaultPolicy tmp(other);
    swap(tmp);
    return *this;
}

DefaultPolicy & DefaultPolicy::operator =(DefaultPolicy && other)
{
    DefaultPolicy tmp(std::move(other));
    swap(tmp);
    return *this;
}

void DefaultPolicy::pinEvent(const Pin & pin, const PinEvent event)
{
    auto & logger = _simulator->getLogger();
    switch (event)
    {
        case PinEvent::PIN_UNINITIALIZED_WRITE:
            logger.warn(pin, PinEvent::PIN_UNINITIALIZED_WRITE);
            break;
        case PinEvent::PIN_UNINITIALIZED_READ:
            logger.warn(pin, PinEvent::PIN_UNINITIALIZED_READ);
            break;
        case PinEvent::PIN_INVALID_WRITE:
            logger.error(pin, PinEvent::PIN_INVALID_WRITE);
            break;
        case PinEvent::PIN_INVALID_READ:
            logger.error(pin, PinEvent::PIN_INVALID_READ);
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_INPUPT:
            logger.error(pin, PinEvent::PIN_INVALID_INITIALIZE_INPUPT);
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_INPUPT_PULLUP:
            logger.error(pin, PinEvent::PIN_INVALID_INITIALIZE_INPUPT_PULLUP);
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_OUTPUT:
            logger.error(pin, PinEvent::PIN_INVALID_INITIALIZE_OUTPUT);
            break;
        case PinEvent::PIN_INVALID_INITIALIZE_VALUE:
            logger.error(pin, PinEvent::PIN_INVALID_INITIALIZE_VALUE);
            break;
        case PinEvent::PIN_INVALID_REINITIALIZE:
            logger.error(pin, PinEvent::PIN_INVALID_REINITIALIZE);
            break;
        case PinEvent::PIN_DOUBLE_WRITE_FALSE:
            logger.warn(pin, PinEvent::PIN_DOUBLE_WRITE_FALSE);
            break;
        case PinEvent::PIN_DOUBLE_WRITE_TRUE:
            logger.warn(pin, PinEvent::PIN_DOUBLE_WRITE_TRUE);
            break;
        case PinEvent::PIN_DOUBLE_INITIALIZE:
            logger.warn(pin, PinEvent::PIN_DOUBLE_INITIALIZE);
            break;
    }
}

namespace arduinoNative {

void swap(DefaultPolicy & p1, DefaultPolicy & p2)
{
    p1.swap(p2);
}

} // arduinoNative
