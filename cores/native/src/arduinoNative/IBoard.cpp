#include "../../include/arduinoNative/IBoard.hpp"
#include "../../include/arduinoNative/Simulator.hpp"

using arduinoNative::IBoard;

IBoard::~IBoard()
{}

IBoard::IBoard():
    IComponent()
{}

IBoard::IBoard(const IBoard & other):
    IComponent(other)
{}

IBoard::IBoard(IBoard && other):
    IComponent(std::move(other))
{}

void IBoard::swap(IBoard & other)
{
    using std::swap;
    IComponent::swap(other);
}
