#include "../../include/arduinoNative/DefaultBoard.hpp"
#include <utility>

using arduinoNative::DefaultBoard;

DefaultBoard::DefaultBoard():
    IBoard()
{}

DefaultBoard::~DefaultBoard()
{}

DefaultBoard::DefaultBoard(const DefaultBoard & other):
    IBoard(other)
{}

DefaultBoard::DefaultBoard(DefaultBoard && other):
    IBoard(std::move(other))
{}

DefaultBoard & DefaultBoard::operator =(const DefaultBoard & other)
{
    auto tmp(other);
    swap(tmp);
    return *this;
}

DefaultBoard & DefaultBoard::operator =(DefaultBoard && other)
{
    swap(other);
    return *this;
}

void DefaultBoard::swap(DefaultBoard & other)
{
    //using std::swap;
    IBoard::swap(other);
}

void DefaultBoard::pinInit(Pin & pin) const
{
    pin.initialize(false, PinMode::MODE_UNINITIALIZED);
}

bool DefaultBoard::pinCheckMode(const Pin & pin) const
{
    switch (pin.getMode())
    {
        case PinMode::MODE_UNINITIALIZED: return false;
        case PinMode::MODE_INVALID: return false;
        case PinMode::MODE_DEFAULT_OUTPUT: return false;
        case PinMode::MODE_DEFAULT_INPUT: return false;
        case PinMode::MODE_DEFAULT_INPUT_PULLUP: return false;
        case PinMode::MODE_OUTPUT: return true;
        case PinMode::MODE_INPUT: return true;
        case PinMode::MODE_INPUT_PULLUP: return true;
    }
    return false;
}

bool DefaultBoard::pinCanReinitialize(const Pin & /*pin*/) const
{
    return false;
}

namespace arduinoNative {

void swap(DefaultBoard & d1, DefaultBoard & d2)
{
    d1.swap(d2);
}

} // arduinoNative
