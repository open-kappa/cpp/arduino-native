#ifndef ARDUINONATIVE_HPP
#define ARDUINONATIVE_HPP

/// @brief Namespace for the simulator.
namespace arduinoNative {}

#include "arduinoNative/config.hpp"
#include "arduinoNative/DefaultBoard.hpp"
#include "arduinoNative/DefaultPolicy.hpp"
#include "arduinoNative/IBoard.hpp"
#include "arduinoNative/IComponent.hpp"
#include "arduinoNative/IPolicy.hpp"
#include "arduinoNative/Logger.hpp"
#include "arduinoNative/Pin.hpp"
#include "arduinoNative/pinTypes.hpp"
#include "arduinoNative/PolicyMode.hpp"
#include "arduinoNative/SimulationEvent.hpp"
#include "arduinoNative/Simulator.hpp"

#endif
