#ifndef ARDUINONATIVE_PINTYPES_HPP
#define ARDUINONATIVE_PINTYPES_HPP

#include <cstdint>

namespace arduinoNative {

/// @brief The possible modes for a pin.
enum class PinMode
{
    /// Uninitialized pin.
    MODE_UNINITIALIZED,
    /// Set in case user pass an invalid constant for mode.
    MODE_INVALID,
    /// Possible value set by a policy.
    MODE_DEFAULT_OUTPUT,
    /// Possible value set by a policy.
    MODE_DEFAULT_INPUT,
    /// Possible value set by a policy.
    MODE_DEFAULT_INPUT_PULLUP,
    /// Output mode.
    MODE_OUTPUT,
    /// Input mode.
    MODE_INPUT,
    /// Input with pullup mode.
    MODE_INPUT_PULLUP
};

/// @brief A pin number.
using PinNumber = uint8_t;

/// @brief Pin possible events.
enum class PinEvent
{
    /// Signal a write, on uninitialized pin.
    PIN_UNINITIALIZED_WRITE,
    /// Signal a read, on uninitialized pin.
    PIN_UNINITIALIZED_READ,
    /// Signal a write, on an input pin.
    PIN_INVALID_WRITE,
    /// Signal a read, on an output pin.
    PIN_INVALID_READ,
    /// Signal an invalid call on initialize as input (setMode).
    PIN_INVALID_INITIALIZE_INPUPT,
    /// Signal an invalid call on initialize as input pullup (setMode).
    PIN_INVALID_INITIALIZE_INPUPT_PULLUP,
    /// Signal an invalid call on initialize as output (setMode).
    PIN_INVALID_INITIALIZE_OUTPUT,
    /// Signal an invalid call on initialize due to the passed value (setMode).
    PIN_INVALID_INITIALIZE_VALUE,
    /// Signal an invalid call on re-initialize (setMode).
    PIN_INVALID_REINITIALIZE,
    /// Signal an double write to false.
    PIN_DOUBLE_WRITE_FALSE,
    /// Signal an double write to true.
    PIN_DOUBLE_WRITE_TRUE,
    /// Signal a re-initialize with the same value.
    PIN_DOUBLE_INITIALIZE
};

} // arduinoNative

#endif
