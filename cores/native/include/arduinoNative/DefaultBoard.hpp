#ifndef ARDUINONATIVE_DEFAULTBOARD_HPP
#define ARDUINONATIVE_DEFAULTBOARD_HPP

#include "config.hpp"
#include "IBoard.hpp"

namespace arduinoNative {

/// @brief Default board.
/// It implements the simplest possible behavior, with basically no checks.
class ARDUINONATIVE_EXPORT DefaultBoard:
    public IBoard
{
public:
    /// @brief Constructor.
    DefaultBoard();
    /// @brief Destructor.
    virtual ~DefaultBoard();
    /// @brief Constructor.
    /// @param other The object to copy.
    DefaultBoard(const DefaultBoard & other);
    /// @brief Constructor.
    /// @param other The object to move.
    DefaultBoard(DefaultBoard && other);
    /// @brief  Assign operator.
    /// @param other The other object.
    /// @return This.
    DefaultBoard & operator =(const DefaultBoard & other);
    /// @brief  Assign move operator.
    /// @param other The other object.
    /// @return This.
    DefaultBoard & operator =(DefaultBoard && other);
    /// @brief Swap.
    /// @param other The object to swap.
    void swap(DefaultBoard & other);
    /// @brief Initialize a pin.
    /// Can set the initial value and the default mode, by calling initialize().
    /// @param pin The pin.
    virtual void pinInit(Pin & pin) const override;
    /// @brief Check the pin mode
    /// @param pin The pin to check.
    /// @return True if pin mode is valid.
    virtual bool pinCheckMode(const Pin & pin) const override;
    /// @brief Check if pin can be re-initialized.
    /// @param pin The pin to check.
    /// @return True if pin can be reinitialized.
    virtual bool pinCanReinitialize(const Pin & pin) const override;
};

void swap(DefaultBoard & d1, DefaultBoard & d2);

} // arduinoNative

#endif
