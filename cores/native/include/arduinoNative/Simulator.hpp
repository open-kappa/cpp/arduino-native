#ifndef ARDUINONATIVE_SIMULATOR_HPP
#define ARDUINONATIVE_SIMULATOR_HPP

#include <map>
#include <memory>
#include "config.hpp"
#include "Pin.hpp"
#include "IPolicy.hpp"
#include "IBoard.hpp"
#include "Logger.hpp"

namespace arduinoNative {

/// @brief The simulator class.
class ARDUINONATIVE_EXPORT Simulator final
{
public:
    /// @brief Singleton.
    /// @return The instance.
    static Simulator & getInstance();

    /// @brief Run the simulation.
    void run();
    /// @brief On SIGINT handler.
    /// This method must be considered as private.
    /// @private
    void _onSigInt();
    /// @brief On SIGTERM handler.
    /// This method must be considered as private.
    /// @private
    void _onSigTerm();

    /// @brief Get the policy.
    /// @return The policy.
    IPolicy & getPolicy();
    /// @brief Set the policy.
    /// Take the ownership.
    /// @param policy The policy.
    void setPolicy(IPolicy * policy);
    /// @brief Get the board.
    /// @return The board.
    IBoard & getBoard();
    /// @brief Set the board.
    /// Take the ownership.
    /// @param board The board.
    void setBoard(IBoard * board);
    /// @brief Get the board.
    /// @return The board.
    Logger & getLogger();

    /// @brief Get a pin.
    /// @param number The pin number to get.
    /// @return The pin.
    Pin & getPin(const PinNumber number);

private:
    /// @brief Constructor.
    Simulator();
    /// @brief Destructor.
    ~Simulator();

    /// @brief Whether to loop or terminate the simulation.
    volatile bool _continue;

    /// @brief The set of current pins.
    std::map<PinNumber, std::shared_ptr<Pin>> _pins;
    /// @brief The current policy.
    IPolicy * _policy;
    /// @brief The board.
    IBoard * _board;
    /// @brief The logger.
    Logger _logger;

    Simulator(const Simulator & other) = delete;
    Simulator(Simulator && other) = delete;
    Simulator & operator =(const Simulator & other) = delete;
    Simulator & operator =(Simulator && other) = delete;
};

} // arduinoNative

#endif
