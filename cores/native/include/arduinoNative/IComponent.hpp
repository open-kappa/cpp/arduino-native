#ifndef ARDUINONATIVE_ICOMPONENT_HPP
#define ARDUINONATIVE_ICOMPONENT_HPP

#include "config.hpp"

namespace arduinoNative {
class Simulator;
} // arduinoNative

namespace arduinoNative {

/// @brief The interface for a simulator component.
class ARDUINONATIVE_EXPORT IComponent
{
public:
    /// @brief Destructor.
    virtual ~IComponent() = 0;
    /// @brief Set the Simulator.
    /// @param simulator The simulator.
    void setSimulator(Simulator * simulator);

protected:
    /// @brief Constructor.
    IComponent();
    /// @brief Constructor.
    /// @param other The object to copy.
    IComponent(const IComponent & other);
    /// @brief Constructor.
    /// @param other The object to move.
    IComponent(IComponent && other);
    /// @brief Swap.
    /// @param other The object to swap.
    void swap(IComponent & other);

protected:
    Simulator * _simulator;

private:
    IComponent & operator =(const IComponent & other) = delete;
    IComponent & operator =(IComponent && other) = delete;
};

} // arduinoNative

#endif
