#ifndef ARDUINONATIVE_IPOLICY_HPP
#define ARDUINONATIVE_IPOLICY_HPP

#include "config.hpp"
#include "IComponent.hpp"
#include "Pin.hpp"
#include "pinTypes.hpp"

namespace arduinoNative {

/// @brief The simulator checks.
class ARDUINONATIVE_EXPORT IPolicy:
    public IComponent
{
public:
    /// @brief Destructor.
    virtual ~IPolicy() = 0;
    /// @brief Check a pin interaction.
    /// @param pin The pin.
    /// @param event The pin event.
    virtual void pinEvent(const Pin & pin, const PinEvent event) = 0;

protected:
    /// @brief Constructor.
    IPolicy();
    /// @brief Constructor.
    /// @param other The object to copy.
    IPolicy(const IPolicy & other);
    /// @brief Constructor.
    /// @param other The object to move.
    IPolicy(IPolicy && other);
    /// @brief Swap.
    /// @param other The object to swap.
    void swap(IPolicy & other);

private:
    IPolicy & operator =(const IPolicy & other) = delete;
    IPolicy & operator =(IPolicy && other) = delete;
};

} // arduinoNative

#endif
