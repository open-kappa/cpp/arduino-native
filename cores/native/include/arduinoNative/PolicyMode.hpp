#ifndef ARDUINONATIVE_POLICYMODE_HPP
#define ARDUINONATIVE_POLICYMODE_HPP


namespace arduinoNative {

/// @brief The behavior of default policy on issues.
enum class PolicyMode
{
    /// @brief Warn, and continue.
    WARN,
    /// @brief Pause execution, to allow inspection.
    PAUSE,
    /// @brief Abort the execution.
    ABORT,
    /// @brief Ignore, and continue.
    IGNORE,
};

} // arduinoNative

#endif
