#ifndef ARDUINONATIVE_IBOARD_HPP
#define ARDUINONATIVE_IBOARD_HPP

#include "config.hpp"
#include "IComponent.hpp"
#include "Pin.hpp"
#include "pinTypes.hpp"

namespace arduinoNative {

/// @brief The interface for a board.
class ARDUINONATIVE_EXPORT IBoard:
    public IComponent
{
public:
    /// @brief Destructor.
    virtual ~IBoard() = 0;
    /// @brief Initialize a pin.
    /// Can set the initial value and the default mode, by calling initialize().
    /// @param pin The pin.
    virtual void pinInit(Pin & pin) const = 0;
    /// @brief Check the pin mode
    /// @param pin The pin to check.
    /// @return True if pin mode is valid.
    virtual bool pinCheckMode(const Pin & pin) const = 0;
    /// @brief Check if pin can be re-initialized.
    /// @param pin The pin to check.
    /// @return True if pin can be reinitialized.
    virtual bool pinCanReinitialize(const Pin & pin) const = 0;

protected:
    /// @brief Constructor.
    IBoard();
    /// @brief Constructor.
    /// @param other The object to copy.
    IBoard(const IBoard & other);
    /// @brief Constructor.
    /// @param other The object to move.
    IBoard(IBoard && other);
    /// @brief Swap.
    /// @param other The object to swap.
    void swap(IBoard & other);

private:
    IBoard & operator =(const IBoard & other) = delete;
    IBoard & operator =(IBoard && other) = delete;
};

} // arduinoNative

#endif
