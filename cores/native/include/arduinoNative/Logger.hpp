#ifndef ARDUINONATIVE_LOGGER_HPP
#define ARDUINONATIVE_LOGGER_HPP

#include "config.hpp"
#include "IComponent.hpp"
#include "pinTypes.hpp"
#include "SimulationEvent.hpp"

namespace arduinoNative {
class ARDUINONATIVE_EXPORT Pin;
}

namespace arduinoNative {

/// @brief The simulator logging interface.
class ARDUINONATIVE_EXPORT Logger final:
    public IComponent
{
public:
    /// @brief Constructor.
    Logger();
    /// @brief Destructor.
    ~Logger();

    /// @brief Print a log message.
    /// @param event The event.
    void log(const SimulationEvent event);
    /// @brief Print a warn message.
    /// @param event The event.
    void warn(const SimulationEvent event);
    /// @brief Print a error message.
    /// @param event The event.
    void error(const SimulationEvent event);

    /// @brief Print a log message.
    /// @param pin The pin.
    /// @param event The event.
    void log(const Pin & pin, const PinEvent event);
    /// @brief Print a warn message.
    /// @param pin The pin.
    /// @param event The event.
    void warn(const Pin & pin, const PinEvent event);
    /// @brief Print a error message.
    /// @param pin The pin.
    /// @param event The event.
    void error(const Pin & pin, const PinEvent event);

private:
    Logger(const Logger & other) = delete;
    Logger(Logger && other) = delete;
    Logger & operator =(const Logger & other) = delete;
    Logger & operator =(Logger && other) = delete;
};

} // arduinoNative

#endif
