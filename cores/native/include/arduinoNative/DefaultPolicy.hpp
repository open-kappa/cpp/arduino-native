#ifndef ARDUINONATIVE_DEFAULTPOLICY_HPP
#define ARDUINONATIVE_DEFAULTPOLICY_HPP

#include <map>
#include "config.hpp"
#include "IPolicy.hpp"
#include "PolicyMode.hpp"

namespace arduinoNative {

/// @brief The simulator default policy.
class ARDUINONATIVE_EXPORT DefaultPolicy final:
    public IPolicy
{
public:
    /// @brief Constructor.
    DefaultPolicy();
    /// @brief Destructor.
    virtual ~DefaultPolicy();
    /// @brief Constructor.
    /// @param other The object to copy.
    DefaultPolicy(const DefaultPolicy & other);
    /// @brief Constructor.
    /// @param other The object to move.
    DefaultPolicy(DefaultPolicy && other);
    /// @brief Swap.
    /// @param other The object to swap.
    void swap(DefaultPolicy & other);
    /// @brief Assign operator.
    /// @param other The object to copy.
    /// @return This.
    DefaultPolicy & operator =(const DefaultPolicy & other);
    /// @brief Assign move operator.
    /// @param other The object to move.
    /// @return This.
    DefaultPolicy & operator =(DefaultPolicy && other);

    virtual void pinEvent(const Pin & pin, const PinEvent event) override;
};

/// @brief Swap two default policies.
/// @param p1 The first policy.
/// @param p2 The second policy.
void swap(DefaultPolicy & p1, DefaultPolicy & p2);

} // arduinoNative

#endif
