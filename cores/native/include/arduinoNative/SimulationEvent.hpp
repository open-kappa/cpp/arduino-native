#ifndef ARDUINONATIVE_SIMULATIONEVENT_HPP
#define ARDUINONATIVE_SIMULATIONEVENT_HPP


namespace arduinoNative {

/// @brief The events raised by the simulator.
enum class SimulationEvent
{
    /// @brief Simulation completed.
    COMPLETED,
    /// @brief Simulation paused.
    PAUSED,
    /// @brief Simulation resumed.
    RESUMED,
    /// @brief Simulation started.
    START
};

} // arduinoNative

#endif
