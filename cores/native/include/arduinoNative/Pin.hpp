#ifndef ARDUINONATIVE_PIN_HPP
#define ARDUINONATIVE_PIN_HPP

#include "config.hpp"
#include "IComponent.hpp"
#include "pinTypes.hpp"

namespace arduinoNative {

/// @brief A pin representation inside the simulator.
class ARDUINONATIVE_EXPORT Pin final:
    public IComponent
{
public:
    /// @brief Comparator for pointers-to-pin objects.
    struct Comparator
    {
        /// @brief The comparison method.
        /// It just wraps the less operator.
        /// @param lhs The first operand.
        /// @param rhs The second operand.
        /// @return The comparison result.
        bool operator()(
            const Pin * const lhs,
            const Pin * const rhs
        ) const
        {
            return *lhs < *rhs;
        }
    };

    /// @brief Constructor.
    /// @param number The pin number.
    Pin(const PinNumber number);
    /// @brief Destructor.
    ~Pin();

    /// @brief Get the pin number.
    /// @return The number.
    PinNumber getNumber() const;

    /// @brief Get the PinMode object.
    /// @return The mode.
    PinMode getMode() const;
    /// @brief Set the mode.
    /// @param mode The mode.
    void setMode(const PinMode mode);

    /// @brief Read the pin value.
    /// @return
    bool read() const;
    /// @brief Set the pin value.
    /// @param value
    void write(const bool value);

    /// @brief Set the initial configuration.
    /// Must be used only by IBoard children classes.
    /// @param value The value.
    /// @param mode The mode.
    void initialize(
        const bool value,
        const PinMode mode
    );

    /// @brief Compares two pin identifiers.
    /// @param other The other pin.
    /// @return The comparison result.
    bool operator <(const Pin & other) const;

private:
    /// @brief The pin number.
    const PinNumber _number;
    /// @brief The pin mode.
    PinMode _mode;
    /// @brief The pin current value.
    bool _value;

    Pin(const Pin & other) = delete;
    Pin(Pin && other) = delete;
    Pin & operator =(const Pin & other) = delete;
    Pin & operator =(Pin && other) = delete;
};

} // arduinoNative

#endif
