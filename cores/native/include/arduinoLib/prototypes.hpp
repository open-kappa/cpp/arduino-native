#ifndef ARDUINOLIB_PROTOTYPES_HPP
#define ARDUINOLIB_PROTOTYPES_HPP

#include "config.hpp"

/// @brief THe Arduino setup method.
ARDUINOLIB_EXTERNAL
void setup();

/// @brief THe Arduino loop method.
ARDUINOLIB_EXTERNAL
void loop();

#endif
