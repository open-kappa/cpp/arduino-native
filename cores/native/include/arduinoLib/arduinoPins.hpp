#ifndef ARDUINOLIB_ARDUINOPINS_HPP
#define ARDUINOLIB_ARDUINOPINS_HPP

#include <cstdint>
#include "config.hpp"

namespace arduinoLib {

constexpr uint8_t OUTPUT = 0;
constexpr uint8_t INPUT = 1;
constexpr uint8_t INPUT_PULLUP = 2;

ARDUINOLIB_EXPORT
void pinMode(const uint8_t pin, const uint8_t mode);
ARDUINOLIB_EXPORT
void digitalWrite(const uint8_t pin, const uint8_t value);
ARDUINOLIB_EXPORT
int digitalRead(const uint8_t pin);

} // arduino

#endif
