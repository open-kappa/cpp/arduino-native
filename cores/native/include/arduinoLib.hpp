#ifndef ARDUINOLIB_ARDUINO_HPP
#define ARDUINOLIB_ARDUINO_HPP

/// @brief Arduino namespace
namespace arduinoLib {}

#include "arduinoLib/arduinoPins.hpp"
#include "arduinoLib/config.hpp"

#endif
