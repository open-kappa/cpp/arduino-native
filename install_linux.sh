#!/usr/bin/env bash

function get_script_dir()
{
    local DIR=""
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
        DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    THIS_SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
}
get_script_dir

INSTALL_DIR="$HOME/.arduino15"
SKETCH_DIR="$HOME/.Arduino"
ARDUINO_NATIVE_VERSION="0.0.0"

function help_n_quit()
{
    echo ""
    echo "install_linux.sh <cmd>"
    echo ""
    echo "CMD:"
    echo "    install: install in .arduino15 folder"
    echo "    uninstall: uninstall from .arduino15 folder"
    echo "    sketch-install: install in sketch folder"
    echo "    sketch-uninstall: uninstall from sketch folder"
    echo ""
    exit 1
}

function doInstall()
{
    local DIR="$INSTALL_DIR/packages/native/hardware/native/$ARDUINO_NATIVE_VERSION"
    cp package/package_native_index.json $INSTALL_DIR/
    mkdir -p $DIR
    cp -r boards.txt cores libraries tools platform.txt programmers.txt variants $DIR
}

function doUninstall()
{
    local DIR="$INSTALL_DIR/packages/native"
    rm -f $INSTALL_DIR/package_native_index.json
    rm -fr $DIR
}

function doSketchInstall()
{
    local DIR="$SKETCH_DIR/hardware/native/native"
    mkdir -p $DIR
    cp -r boards.txt cores libraries tools platform.txt programmers.txt variants $DIR
}

function doSketchUninstall()
{
    local DIR="$SKETCH_DIR/hardware/native"
    rm -fr $DIR
}

cd $THIS_SCRIPT_DIR
case "$1" in
    install)
        doInstall
        ;;
    uninstall)
        doUninstall
        ;;
    sketch-install)
        doSketchInstall
        ;;
    sketch-uninstall)
        doSketchUninstall
        ;;
    *)
        help_n_quit
        ;;
esac

# EOF
