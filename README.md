# ARDUINO NATIVE

A library to compile Arduino projects to native host executables.

## Benefits

It is easy to use profilers, debuggers, sanity checkers for native code,
whilst it is almost impossible for target embedded code.

## Installation

1. Open the `install_linux.sh` script.
1. Customize the `INSTALL_DIR` variable.
1. Run the script

## Disinstallation

1. Run `install_linux.sh --uninstall`
