#ifndef ARDUINO_NATIVE_CUSTOM_MAIN
#include <cstdlib>
#include <include/arduinoNative.hpp>

using arduinoNative::DefaultBoard;
using arduinoNative::DefaultPolicy;
using arduinoNative::Simulator;
using arduinoNative::SimulationEvent;

int main()
{
    auto & sim = Simulator::getInstance();
    sim.getLogger().log(SimulationEvent::START);
    sim.setPolicy(new DefaultPolicy());
    sim.setBoard(new DefaultBoard());
    sim.run();
    sim.getLogger().log(SimulationEvent::COMPLETED);
    return EXIT_SUCCESS;
}

#endif
